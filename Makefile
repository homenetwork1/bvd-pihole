
dev:
	docker-compose -f docker-compose.yml up

prod:
	docker-compose -f docker-compose.yml up

prod-d:
	docker-compose -f docker-compose.yml up -d

build:
	docker-compose -f docker-compose.yml build

build-clean:
	docker-compose -f docker-compose.yml build --no-cache

bash-pihole:
	docker exec -it pihole bash
