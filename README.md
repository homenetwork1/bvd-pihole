# bVd PiHole

Internal Pihole project

## Setup Pi

- change user
- static ip
- change password
- enable ssh keys
- auto update

## Install application pihole

clone git repo

```bash
make prod-d
```

## add domain redirect

```bash
make bash-pihole
pihole -a hostrecord cloud.bvdcreations.com 192.168.x.x
```

